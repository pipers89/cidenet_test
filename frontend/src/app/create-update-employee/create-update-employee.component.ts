import {
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EmployeeService } from '../services/employee.service';
import Swal from 'sweetalert2';
import { Employee } from '../interfaces/employee.interface';
import { ObjDeepCopy } from '../utils/utils';

@Component({
  selector: 'app-create-update-employee',
  templateUrl: './create-update-employee.component.html',
  styleUrls: ['./create-update-employee.component.css'],
})
export class CreateUpdateEmployeeComponent implements OnInit, OnDestroy {
  @Input() public labelNameModal!: string;
  @Input() public idEmployee!: number;
  @Output() createOrUpdateEmployee = new EventEmitter<boolean>();

  public areas: any[] = [];
  public countries: any[] = [];
  public typeIdentifications: any[] = [];
  public employeeForm: FormGroup = this.FB.group({
    Id: [undefined],
    FirstName: [undefined, [Validators.required, Validators.max(20)]],
    SecondName: [undefined, Validators.max(50)],
    Surname: [undefined, [Validators.required, Validators.max(20)]],
    SecondSurname: [undefined, [Validators.required, Validators.max(20)]],
    IdentificationNumber: [undefined, [Validators.maxLength(20)]],
    Email: [undefined],
    IdCountry: [undefined],
    IdArea: [undefined],
    IdIdentification: [undefined],
    AdmissionDate: [undefined],
    State: [{ value: true, disabled: true }],
  });

  constructor(
    private employeeService: EmployeeService,
    private FB: FormBuilder
  ) {}

  ngOnDestroy(): void {
    this.employeeForm.reset();
    this.idEmployee = 0;
  }

  public async ngOnInit() {
    if (this.idEmployee !== 0 && this.idEmployee !== undefined) {
      const resEmployeeById = await this.employeeService.getEmployeeById(
        this.idEmployee
      );

      if (resEmployeeById !== null) {
        let employee: Employee = resEmployeeById;
        employee.IdArea = resEmployeeById?.Area?.Id;
        employee.IdCountry = resEmployeeById?.Country?.Id;
        employee.IdIdentification = resEmployeeById?.Identification?.Id;
        employee.State = resEmployeeById.State === 1 ? true : false;
        this.employeeForm.patchValue(employee);
      }
    }

    await this.getAreas();
    await this.getCountries();
    await this.getTypeIdentifications();
  }

  public async validateAction() {
    if (this.employeeForm.valid) {
      let model: Employee = ObjDeepCopy(this.employeeForm.value);
      if (this.labelNameModal === 'Crear') {
        model.Id = 0;
        const resSave = await this.employeeService.saveEmployee(model);
        if (resSave !== null) {
          Swal.fire(
            'Registro!',
            'Empleado registrado correctamente.',
            'success'
          );
        }
      } else {
        const resUpdate = await this.employeeService.updateEmployee(model);

        if (resUpdate !== null) {
          Swal.fire(
            'Registro!',
            'Empleado actualizado correctamente.',
            'success'
          );
        }
      }

      this.employeeForm.reset();
      this.idEmployee = 0;
      this.createOrUpdateEmployee.emit(true);
    }
  }

  public async getAreas() {
    const resAreas = await this.employeeService.getAllAreas();

    if (resAreas !== null && resAreas.length > 0) {
      this.areas = resAreas;
    }
  }

  public async getTypeIdentifications() {
    const resTypeIdentifications =
      await this.employeeService.getAllTypeIdentifications();

    if (resTypeIdentifications !== null && resTypeIdentifications.length > 0) {
      this.typeIdentifications = resTypeIdentifications;
    }
  }

  public async getCountries() {
    const resCountries = await this.employeeService.getAllCountries();

    if (resCountries !== null && resCountries.length > 0) {
      this.countries = resCountries;
    }
  }
}
