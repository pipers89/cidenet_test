import { Component, OnInit, PipeTransform } from '@angular/core';
import { Employee } from '../interfaces/employee.interface';
import { DecimalPipe } from '@angular/common';
import { FormControl } from '@angular/forms';

import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import Swal from 'sweetalert2';
import { EmployeeService } from '../services/employee.service';

import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-list-employee',
  templateUrl: './list-employee.component.html',
  styleUrls: ['./list-employee.component.css'],
})
export class ListEmployeeComponent implements OnInit {
  public employeeList: Employee[] = [];
  public countries$!: Observable<Employee[]>;
  filter = new FormControl('');
  public labelNameModal: string = '';
  public page: number;
  public countCollections: number = 0;
  public pageSize: number;
  public idEmployee: number = 0;

  constructor(
    private employeeService: EmployeeService,
    private modalService: NgbModal
  ) {
    this.pageSize = 5;
    this.page = 1;
  }

  public async ngOnInit() {
    await this.refresh();
  }

  public async refresh() {
    await this.getAllEmployees();
    this.countries$ = this.filter.valueChanges.pipe(
      startWith(''),
      map((text) => this.searchEmployee(text))
    );
  }

  /**
   *
   * @param text
   * @returns List of employees
   */
  public searchEmployee(text: string): Employee[] {
    this.refreshEmployeesList();
    return this.employeeList.filter((employee: Employee) => {
      const term = text.toLowerCase();
      return (
        employee.FirstName?.toLowerCase().includes(term) ||
        employee.SecondName?.toLowerCase().includes(term) ||
        employee.Surname?.toLowerCase().includes(term) ||
        employee.SecondSurname?.toLowerCase().includes(term) ||
        employee.Email?.toLowerCase().includes(term)
      );
    });
  }

  /**
   * @description Refresh the list of employees
   */
  public refreshEmployeesList() {
    this.employeeList = this.employeeList
      .map((e, i) => ({ id: i + 1, ...e }))
      .slice(
        (this.page - 1) * this.pageSize,
        (this.page - 1) * this.pageSize + this.pageSize
      );
  }

  /**
   * @description open a new modal window and identify with case apply
   * @param content
   * @param employee
   */
  public async openNewOrUpdateEmployee(content: any, employee?: Employee) {
    this.identifyAction(employee);
    this.modalService
      .open(content, { size: 'lg' })
      .dismissed.subscribe((result: any) => {
        this.idEmployee = 0;
      });
  }

  /**
   * @description Identify it's create employee or update
   * @param employee
   */
  public async identifyAction(employee?: Employee) {
    if (employee?.Id !== undefined && employee?.Id !== 0) {
      this.labelNameModal = 'Actualizar';
      this.idEmployee = employee?.Id;
    } else {
      this.labelNameModal = 'Crear';
    }
  }

  /**
   * @description Alert window when user clic on the button action delete
   * @param idEmployee
   */
  public async deleteEmployee(idEmployee: number) {
    Swal.fire({
      title: 'Eliminar',
      text: 'Está seguro de que desea eliminar el empleado? ',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si',
      cancelButtonText: 'No',
    }).then((result) => {
      if (result.value) {
        this.employeeService.deleteEmployee(idEmployee).then(() => {
          Swal.fire(
            'Eliminado!',
            'El registro se elimino exitosamente.',
            'success'
          );
          this.refresh();
        });
      }
    });
  }

  public async createOrUpdateSuccess(modal: any) {
    modal.dismiss();
    this.refresh();
  }

  /**
   * @description get all employees register into DB
   */
  public async getAllEmployees() {
    const resAllEmployee: Employee[] =
      await this.employeeService.getAllEmployees();

    if (resAllEmployee !== null && resAllEmployee.length > 0) {
      this.employeeList = resAllEmployee;
      this.countCollections = resAllEmployee.length;
    } else {
      this.employeeList = [];
      this.countCollections = 0;
    }
  }
}
