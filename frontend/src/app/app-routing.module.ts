import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateUpdateEmployeeComponent } from './create-update-employee/create-update-employee.component';
import { ListEmployeeComponent } from './list-employee/list-employee.component';

const routes: Routes = [
  {
    path: 'list-employee',
    component: ListEmployeeComponent,
  },
  {
    path: 'create',
    component: CreateUpdateEmployeeComponent,
  },
  {
    path: 'update',
    component: CreateUpdateEmployeeComponent,
  },
  {
    path: '',
    redirectTo: '/list-employee',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
