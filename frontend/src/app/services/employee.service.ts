import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Employee } from '../interfaces/employee.interface';

@Injectable()
export class EmployeeService {
  constructor(private http: HttpClient) {}

  public saveEmployee(employee: Employee): any {
    return this.http
      .post(`api/Employees`, employee)
      .toPromise()
      .then((res) => res);
  }

  public updateEmployee(employee: Employee): any {
    return this.http
      .put(`api/Employees`, employee)
      .toPromise()
      .then((res) => res);
  }

  public deleteEmployee(id: number): any {
    return this.http
      .delete(`api/Employees/${id}`)
      .toPromise()
      .then((res) => res);
  }

  public async getAllEmployees(): Promise<any> {
    let URL: string = `api/Employees`;

    // if (firstName !== '' && firstName !== undefined) {
    //   URL += `offset=${ofset}&`;
    // }
    // if (secondName !== '' && secondName !== undefined) {
    //   URL += `secondName=${firstName}&`;
    // }
    // if (surname !== '' && surname !== undefined) {
    //   URL += `surname=${firstName}&`;
    // }
    // if (secondSurname !== '' && secondSurname !== undefined) {
    //   URL += `secondSurname=${firstName}&`;
    // }
    // if (country !== '' && country !== undefined) {
    //   URL += `country=${firstName}&`;
    // }
    // if (ofset !== undefined && ofset > 0) {
    //   URL += `ofset=${firstName}&`;
    // }

    return this.http
      .get(`${URL}`)
      .toPromise()
      .then((res: any) => res);
  }

  public async getAllAreas(): Promise<any> {
    let URL: string = `api/Areas`;

    return this.http
      .get(`${URL}`)
      .toPromise()
      .then((res: any) => res);
  }

  public async getAllCountries(): Promise<any> {
    let URL: string = `api/Countries`;

    return this.http
      .get(`${URL}`)
      .toPromise()
      .then((res: any) => res);
  }

  public async getAllTypeIdentifications(): Promise<any> {
    let URL: string = `api/TypeIdentifications`;

    return this.http
      .get(`${URL}`)
      .toPromise()
      .then((res: any) => res);
  }

  public getEmployeeById(id: number) {
    let URL: string = `api/Employees/${id}`;

    return this.http
      .get(`${URL}`)
      .toPromise()
      .then((res: any) => res);
  }
}
