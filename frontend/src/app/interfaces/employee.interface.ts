export interface Employee {
  Id?: number;
  FirstName?: string;
  SecondName?: string;
  Surname?: string;
  SecondSurname?: string;
  Email?: string;
  AdmissionDate?: Date;
  State?: boolean;
  IdentificationNumber?: string;
  CreatedAt?: Date;
  IdCountry?: number;
  IdArea?: number;
  IdIdentification?: number;
}
