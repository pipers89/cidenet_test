﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using BackCedenet.Models;
using BackCedenet.CustomModels;

namespace BackCedenet.Controllers
{
    public class TypeIdentificationsController : ApiController
    {
        private readonly ModelCidenet DB = new ModelCidenet();

        // GET: api/TypeIdentifications
        public List<CustomIdentification> GetTypeIdentifications()
        {
            return DB.TypeIdentifications.Select(ti => new CustomIdentification
            {
                Id = ti.Id,
                NameIdentification = ti.nameIdentification
            }).ToList();
        }

        // GET: api/TypeIdentifications/5
        [ResponseType(typeof(TypeIdentification))]
        public IHttpActionResult GetTypeIdentification(int id)
        {
            TypeIdentification typeIdentification = DB.TypeIdentifications.Find(id);
            if (typeIdentification == null)
            {
                return NotFound();
            }

            return Ok(typeIdentification);
        }

        // PUT: api/TypeIdentifications/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutTypeIdentification(int id, TypeIdentification typeIdentification)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != typeIdentification.Id)
            {
                return BadRequest();
            }

            DB.Entry(typeIdentification).State = EntityState.Modified;

            try
            {
                DB.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TypeIdentificationExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/TypeIdentifications
        [ResponseType(typeof(TypeIdentification))]
        public IHttpActionResult PostTypeIdentification(TypeIdentification typeIdentification)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            DB.TypeIdentifications.Add(typeIdentification);
            DB.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = typeIdentification.Id }, typeIdentification);
        }

        // DELETE: api/TypeIdentifications/5
        [ResponseType(typeof(TypeIdentification))]
        public IHttpActionResult DeleteTypeIdentification(int id)
        {
            TypeIdentification typeIdentification = DB.TypeIdentifications.Find(id);
            if (typeIdentification == null)
            {
                return NotFound();
            }

            DB.TypeIdentifications.Remove(typeIdentification);
            DB.SaveChanges();

            return Ok(typeIdentification);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                DB.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TypeIdentificationExists(int id)
        {
            return DB.TypeIdentifications.Count(e => e.Id == id) > 0;
        }
    }
}