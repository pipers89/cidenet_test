﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using BackCedenet.Models;

namespace BackCedenet.Controllers
{
    public class ConsecutiveNumbersController : ApiController
    {
        private readonly ModelCidenet DB = new ModelCidenet();

        public ConsecutiveNumbersController() { }

        // GET: api/ConsecutiveNumbers
        public IQueryable<ConsecutiveNumber> GetConsecutiveNumbers()
        {
            return DB.ConsecutiveNumbers;
        }

        // GET: api/ConsecutiveNumbers/5
        [ResponseType(typeof(ConsecutiveNumber))]
        public IHttpActionResult GetConsecutiveNumber(int id)
        {
            ConsecutiveNumber consecutiveNumber = DB.ConsecutiveNumbers.Find(id);
            if (consecutiveNumber == null)
            {
                return NotFound();
            }

            return Ok(consecutiveNumber);
        }

        [ResponseType(typeof(ConsecutiveNumber))]
        public IHttpActionResult GetConsecutiveNumberByEmployeeId(int IdEmployee)
        {
            ConsecutiveNumber consecutiveNumber = DB.ConsecutiveNumbers.Where(cn => cn.IdEmployee == IdEmployee).OrderByDescending(cn => cn.CreatedAt).FirstOrDefault();
            if (consecutiveNumber == null)
            {
                return NotFound();
            }

            return Ok(consecutiveNumber);
        }

        // PUT: api/ConsecutiveNumbers/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutConsecutiveNumber(int id, ConsecutiveNumber consecutiveNumber)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != consecutiveNumber.Id)
            {
                return BadRequest();
            }

            DB.Entry(consecutiveNumber).State = EntityState.Modified;

            try
            {
                DB.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ConsecutiveNumberExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/ConsecutiveNumbers
        [ResponseType(typeof(ConsecutiveNumber))]
        public IHttpActionResult PostConsecutiveNumber(ConsecutiveNumber consecutiveNumber)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            DB.ConsecutiveNumbers.Add(consecutiveNumber);
            DB.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = consecutiveNumber.Id }, consecutiveNumber);
        }

        // DELETE: api/ConsecutiveNumbers/5
        [ResponseType(typeof(ConsecutiveNumber))]
        public IHttpActionResult DeleteConsecutiveNumber(int id)
        {
            ConsecutiveNumber consecutiveNumber = DB.ConsecutiveNumbers.Find(id);
            if (consecutiveNumber == null)
            {
                return NotFound();
            }

            DB.ConsecutiveNumbers.Remove(consecutiveNumber);
            DB.SaveChanges();

            return Ok(consecutiveNumber);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                DB.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ConsecutiveNumberExists(int id)
        {
            return DB.ConsecutiveNumbers.Count(e => e.Id == id) > 0;
        }
    }
}