﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using BackCedenet.Models;
using BackCedenet.CustomModels;

namespace BackCedenet.Controllers
{
    public class AreasController : ApiController
    {
        private readonly ModelCidenet DB = new ModelCidenet();

        // GET: api/areas
        public List<CustomArea> Getareas()
        {
            return DB.Areas.Select(a => new CustomArea 
            { 
                Id = a.Id,
                NameArea = a.nameArea
            }).ToList();
        }

        // GET: api/areas/5
        [ResponseType(typeof(CustomArea))]
        public IHttpActionResult Getarea(int id)
        {
            Area findArea = DB.Areas.Find(id);
            CustomArea areaCustom = ObjArea(findArea);

            if (findArea == null)
            {
                return NotFound();
            }

            return Ok(areaCustom);
        }

        // PUT: api/areas/5
        [ResponseType(typeof(void))]
        public IHttpActionResult Putarea(int id, Area area)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != area.Id)
            {
                return BadRequest();
            }

            DB.Entry(area).State = EntityState.Modified;

            try
            {
                DB.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AreaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/areas
        [ResponseType(typeof(Area))]
        public IHttpActionResult Postarea(Area area)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            DB.Areas.Add(area);
            DB.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = area.Id }, area);
        }

        // DELETE: api/areas/5
        [ResponseType(typeof(CustomArea))]
        public IHttpActionResult Deletearea(int id)
        {
            Area findArea = DB.Areas.Find(id);
            CustomArea areaCustom = ObjArea(findArea);

            if (areaCustom == null)
            {
                return NotFound();
            }

            DB.Areas.Remove(findArea);
            DB.SaveChanges();

            return Ok(areaCustom);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                DB.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool AreaExists(int id)
        {
            return DB.Areas.Count(e => e.Id == id) > 0;
        }

        private CustomArea ObjArea(Area area)
        {
            return new CustomArea
            {
                Id = area.Id,
                NameArea = area.nameArea
            };
        }
    }
}