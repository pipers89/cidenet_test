﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using BackCedenet.Models;
using BackCedenet.CustomModels;

namespace BackCedenet.Controllers
{
    public class CountriesController : ApiController
    {
        private readonly ModelCidenet DB = new ModelCidenet();

        // GET: api/Countries
        public List<CustomCountry> GetCountries()
        {
            return DB.Countries.Select(c => new CustomCountry
            {
                Id = c.Id,
                NameCountry = c.NameCountry,
                ShortName = c.ShortName
            }).ToList();
        }

        // GET: api/Countries/5
        [ResponseType(typeof(Country))]
        public IHttpActionResult GetCountry(int id)
        {
            Country FindCountry = DB.Countries.Find(id);
            CustomCountry country = ObjCountry(FindCountry);

            if (FindCountry == null)
            {
                return NotFound();
            }

            return Ok(country);
        }

        // PUT: api/Countries/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutCountry(int id, Country country)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != country.Id)
            {
                return BadRequest();
            }

            DB.Entry(country).State = EntityState.Modified;

            try
            {
                DB.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CountryExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Countries
        [ResponseType(typeof(Country))]
        public IHttpActionResult PostCountry(Country country)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            DB.Countries.Add(country);
            DB.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = country.Id }, country);
        }

        // DELETE: api/Countries/5
        [ResponseType(typeof(CustomCountry))]
        public IHttpActionResult DeleteCountry(int id)
        {
            Country FindCountry = DB.Countries.Find(id);
            CustomCountry country = ObjCountry(FindCountry);

            if (FindCountry == null)
            {
                return NotFound();
            }

            DB.Countries.Remove(FindCountry);
            DB.SaveChanges();

            return Ok(country);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                DB.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CountryExists(int id)
        {
            return DB.Countries.Count(e => e.Id == id) > 0;
        }

        private CustomCountry ObjCountry(Country country)
        {
            return new CustomCountry
            {
                Id = country.Id,
                NameCountry = country.NameCountry,
                ShortName = country.ShortName
            };
        }
    }
}