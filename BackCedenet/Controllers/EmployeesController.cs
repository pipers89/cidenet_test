﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using BackCedenet.Models;
using BackCedenet.CustomModels;

namespace BackCedenet.Controllers
{
    public class EmployeesController : ApiController
    {
        private readonly ModelCidenet DB = new ModelCidenet();
        private readonly ConsecutiveNumbersController CTRLConsecutiveNumber = null;

        public EmployeesController()
        {
            CTRLConsecutiveNumber = new ConsecutiveNumbersController();
        }

        // GET: api/Employees
        public List<CustomEmployee> GetEmployees()
        {
            return DB.Employees.Select(emp => new CustomEmployee
            {
                Id = emp.Id,
                FirstName = emp.FirstName,
                SecondName = emp.SecondName,
                Surname = emp.Surname,
                SecondSurname = emp.SecondSurname,
                Email = emp.Email,
                AdmissionDate = emp.AdmissionDate,
                State = emp.State,
                IdentificationNumber = emp.IdentificationNumber,
                CreatedAt = emp.CreatedAt,
                Area = new CustomArea
                {
                    Id = emp.Area.Id,
                    NameArea = emp.Area.nameArea
                },
                Country = new CustomCountry
                {
                    Id = emp.Country.Id,
                    NameCountry = emp.Country.NameCountry,
                    ShortName = emp.Country.ShortName
                },
                Identification = new CustomIdentification
                {
                    Id = emp.TypeIdentification.Id,
                    NameIdentification = emp.TypeIdentification.nameIdentification
                }
            }).ToList();
        }

        // GET: api/Employees/5
        [ResponseType(typeof(CustomEmployee))]
        public IHttpActionResult GetEmployee(int id)
        {
            Employee findEmployee = DB.Employees.Find(id);
            CustomEmployee employee = ObjCusEmployee(findEmployee);

            if (findEmployee == null)
            {
                return NotFound();
            }

            return Ok(employee);
        }

        /// <summary>
        /// Get employee by email
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        [ResponseType(typeof(CustomEmployee))]
        public IHttpActionResult GetEmployeeByEmail(string email)
        {
            Employee findEmployee = DB.Employees.Where(em => em.Email == email).FirstOrDefault();
            CustomEmployee employee = null;

            if (findEmployee == null)
            {
                return null;
            }

            employee = ObjCusEmployee(findEmployee);

            return Ok(employee);
        }

        // PUT: api/Employees/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutEmployee(Employee employee)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            employee.CreatedAt = DateTime.Now;
            employee.State = 1;
            employee.Area = DB.Areas.Find(employee.IdArea);
            employee.Country = DB.Countries.Find(employee.IdCountry);
            employee.TypeIdentification = DB.TypeIdentifications.Find(employee.IdIdentification);
            DB.Entry(employee).State = EntityState.Modified;

            try
            {
                DB.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EmployeeExists(employee.Email))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Employees
        [ResponseType(typeof(Employee))]
        public IHttpActionResult PostEmployee(Employee employee)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            employee.CreatedAt = DateTime.Now;
            employee.State = 1;
            employee.Area = DB.Areas.Find(employee.IdArea);
            employee.Country = DB.Countries.Find(employee.IdCountry);
            employee.TypeIdentification = DB.TypeIdentifications.Find(employee.IdIdentification);
            employee.Email = GenerateUniqueEmail(employee);
            DB.Employees.Add(employee);

            try
            {
                DB.SaveChanges();
            }
            catch (Exception ex)
            {
                if (EmployeeExists(employee.Email))
                {
                    return Conflict();
                }
                else
                {
                    throw ex;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = employee.Id }, employee);
        }

        // DELETE: api/Employees/5
        [ResponseType(typeof(CustomEmployee))]
        public IHttpActionResult DeleteEmployee(int id)
        {
            Employee findEmployee = DB.Employees.Find(id);
            CustomEmployee employee = ObjCusEmployee(findEmployee);

            if (employee == null)
            {
                return NotFound();
            }

            DB.Employees.Remove(findEmployee);
            DB.SaveChanges();

            return Ok(employee);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                DB.Dispose();
            }
            base.Dispose(disposing);
        }

        /// <summary>
        /// Verify if employee exits with this email
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        private bool EmployeeExists(string email)
        {
            return DB.Employees.Count(e => e.Email == email) > 0;
        }

        /// <summary>
        /// Mapping object of type employee
        /// </summary>
        /// <param name="emp"></param>
        /// <returns></returns>
        private CustomEmployee ObjCusEmployee(Employee emp)
        {
            return new CustomEmployee
            {
                Id = emp.Id,
                FirstName = emp.FirstName,
                SecondName = emp.SecondName,
                Surname = emp.Surname,
                SecondSurname = emp.SecondSurname,
                Email = emp.Email,
                AdmissionDate = emp.AdmissionDate,
                State = emp.State,
                IdentificationNumber = emp.IdentificationNumber,
                CreatedAt = emp.CreatedAt,
                Area = new CustomArea
                {
                    Id = emp.Area.Id,
                    NameArea = emp.Area.nameArea
                },
                Country = new CustomCountry
                {
                    Id = emp.Country.Id,
                    NameCountry = emp.Country.NameCountry,
                    ShortName = emp.Country.ShortName
                },
                Identification = new CustomIdentification
                {
                    Id = emp.TypeIdentification.Id,
                    NameIdentification = emp.TypeIdentification.nameIdentification
                }
            };
        }

        /// <summary>
        /// Generate email from information of employee
        /// </summary>
        /// <param name="employee"></param>
        /// <returns></returns>
        private string GenerateUniqueEmail(Employee employee)
        {
            const string DOMAIN_CO = "cidenet.com.co ";
            const string DOMAIN_US = "cidenet.com.us ";
            ConsecutiveNumber objCN = new ConsecutiveNumber();
            long consecutiveNumber;
            Country findCountry = DB.Countries.Find(employee.IdCountry);
            string emailGenerated = $"{employee.FirstName}.{employee.Surname}@{(findCountry.ShortName == "co" ? DOMAIN_CO : DOMAIN_US)}";
            var employeeFounded = (CustomEmployee)GetEmployeeByEmail(emailGenerated);

            if (employeeFounded != null && employeeFounded.Id > 0)
            {
                var cn = (ConsecutiveNumber)CTRLConsecutiveNumber.GetConsecutiveNumberByEmployeeId(employeeFounded.Id);

                if (cn != null && cn.Id > 0)
                {
                    consecutiveNumber = cn.ConsecutiveNumber1 + 1;
                    emailGenerated = $"{employee.FirstName}.{employee.Surname}.{consecutiveNumber}@{(findCountry.ShortName == "co" ? DOMAIN_CO : DOMAIN_US)}";
                }
                else
                {
                    consecutiveNumber = 1;
                    emailGenerated = $"{employee.FirstName}.{employee.Surname}.{consecutiveNumber}@{(findCountry.ShortName == "co" ? DOMAIN_CO : DOMAIN_US)}";
                }

                objCN.IdEmployee = employeeFounded.Id;
                objCN.CreatedAt = DateTime.Now;
                objCN.ConsecutiveNumber1 = consecutiveNumber;
                CTRLConsecutiveNumber.PostConsecutiveNumber(objCN);
            }

            return emailGenerated;
        }
    }
}