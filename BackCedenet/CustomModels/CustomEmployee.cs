﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BackCedenet.CustomModels
{
    public class CustomEmployee
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string Surname { get; set; }
        public string SecondSurname { get; set; }
        public string Email { get; set; }
        public DateTime? AdmissionDate { get; set; }
        public long? State { get; set; }
        public string IdentificationNumber { get; set; }
        public DateTime CreatedAt { get; set; }
        public CustomArea Area { get; set; }
        public CustomCountry Country { get; set; }
        public CustomIdentification Identification { get; set; }
    }
}