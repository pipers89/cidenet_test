﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BackCedenet.CustomModels
{
    public class CustomIdentification
    {
        public int Id { get; set; }
        public string NameIdentification { get; set; }
    }
}