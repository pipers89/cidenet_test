﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BackCedenet.CustomModels
{
    public class CustomArea
    {
        public int Id { get; set; }
        public string NameArea { get; set; }
    }
}