﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BackCedenet.CustomModels
{
    public class CustomCountry
    {
        public int Id { get; set; }
        public string NameCountry { get; set; }
        public string ShortName { get; set; }
    }
}