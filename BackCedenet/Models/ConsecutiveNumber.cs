namespace BackCedenet.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ConsecutiveNumber")]
    public partial class ConsecutiveNumber
    {
        public int Id { get; set; }

        [Column("ConsecutiveNumber")]
        public long ConsecutiveNumber1 { get; set; }

        public int IdEmployee { get; set; }

        public DateTime CreatedAt { get; set; }

        public virtual Employee Employee { get; set; }
    }
}
