using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;

namespace BackCedenet.Models
{
    public partial class ModelCidenet : DbContext
    {
        public ModelCidenet()
            : base("name=ModelCidenet")
        {
        }

        public virtual DbSet<Area> Areas { get; set; }
        public virtual DbSet<ConsecutiveNumber> ConsecutiveNumbers { get; set; }
        public virtual DbSet<Country> Countries { get; set; }
        public virtual DbSet<Employee> Employees { get; set; }
        public virtual DbSet<TypeIdentification> TypeIdentifications { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Area>()
                .Property(e => e.nameArea)
                .IsUnicode(false);

            modelBuilder.Entity<Area>()
                .HasMany(e => e.Employees)
                .WithRequired(e => e.Area)
                .HasForeignKey(e => e.IdArea)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Country>()
                .Property(e => e.NameCountry)
                .IsUnicode(false);

            modelBuilder.Entity<Country>()
                .Property(e => e.ShortName)
                .IsUnicode(false);

            modelBuilder.Entity<Country>()
                .HasMany(e => e.Employees)
                .WithRequired(e => e.Country)
                .HasForeignKey(e => e.IdCountry)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Employee>()
                .Property(e => e.FirstName)
                .IsUnicode(false);

            modelBuilder.Entity<Employee>()
                .Property(e => e.SecondName)
                .IsUnicode(false);

            modelBuilder.Entity<Employee>()
                .Property(e => e.Surname)
                .IsUnicode(false);

            modelBuilder.Entity<Employee>()
                .Property(e => e.SecondSurname)
                .IsUnicode(false);

            modelBuilder.Entity<Employee>()
                .Property(e => e.Email)
                .IsUnicode(false);

            modelBuilder.Entity<Employee>()
                .Property(e => e.IdentificationNumber)
                .IsUnicode(false);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.ConsecutiveNumbers)
                .WithRequired(e => e.Employee)
                .HasForeignKey(e => e.IdEmployee)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TypeIdentification>()
                .Property(e => e.nameIdentification)
                .IsUnicode(false);

            modelBuilder.Entity<TypeIdentification>()
                .HasMany(e => e.Employees)
                .WithRequired(e => e.TypeIdentification)
                .HasForeignKey(e => e.IdIdentification)
                .WillCascadeOnDelete(false);
        }
    }
}
