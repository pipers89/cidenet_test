namespace BackCedenet.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Employee")]
    public partial class Employee
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Employee()
        {
            ConsecutiveNumbers = new HashSet<ConsecutiveNumber>();
        }

        public int Id { get; set; }

        [Required]
        [StringLength(20)]
        public string FirstName { get; set; }

        [StringLength(50)]
        public string SecondName { get; set; }

        [Required]
        [StringLength(20)]
        public string Surname { get; set; }

        [Required]
        [StringLength(20)]
        public string SecondSurname { get; set; }

        [StringLength(300)]
        public string Email { get; set; }

        public DateTime? AdmissionDate { get; set; }

        public long? State { get; set; }

        [Required]
        [StringLength(20)]
        public string IdentificationNumber { get; set; }

        public DateTime CreatedAt { get; set; }

        public int IdCountry { get; set; }

        public int IdArea { get; set; }

        public int IdIdentification { get; set; }

        public virtual Area Area { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ConsecutiveNumber> ConsecutiveNumbers { get; set; }

        public virtual Country Country { get; set; }

        public virtual TypeIdentification TypeIdentification { get; set; }
    }
}
